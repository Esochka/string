package com.test;

import com.company.Main;
import org.junit.Assert;
import org.junit.Test;

public class mainTest {
    @Test
    public void testIntToStringTest() {
        String actual = "54321";

        String expected = Main.reverseString("12345");

        Assert.assertEquals(actual, expected);

    }
}
