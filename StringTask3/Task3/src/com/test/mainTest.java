package com.test;

import com.company.Task3;
import org.junit.Assert;
import org.junit.Test;

public class mainTest {
    @Test
    public void testIntToStringTest() {
        String actual = "1 2, 3";

        String expected = Task3.addSpace("1 2,3");

        Assert.assertEquals(actual, expected);

    }
}
