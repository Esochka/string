package com.test;

import com.company.Task1;
import org.junit.Assert;
import org.junit.Test;

public class mainTest {
    @Test
    public void testIntToStringTest() {
        int actual = 1;

        int expected = Task1.lengthOfWordInString("52 3 12");

        Assert.assertEquals(actual, expected);

    }
}
