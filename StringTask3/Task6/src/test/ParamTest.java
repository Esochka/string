package test;


import com.company.Main;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParamTest {
    private String valueA;
    private Integer valueB;
    private Integer valueC;
    private String expected;

    public ParamTest(String valueA, Integer valueB,Integer valueC,  String expected) {
        this.valueA = valueA;
        this.valueB = valueB;
        this.valueC = valueC;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"1232123" ,3,5, "12323"},
                { "1232123" ,1,3,"12123"},

        });
    }

    @Test
    public void delete() {
        assertEquals(expected,  new Main().delete(valueA, valueB, valueC));
    }


}
