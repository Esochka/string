package test;


import com.company.Main;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParamTest {
    private String valueA;

    private String expected;

    public ParamTest(String valueA,  String expected) {
        this.valueA = valueA;

        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"12345 1" , "12345  "},
                { "2345 33223" , "2345  "},

        });
    }

    @Test
    public void reverseStringTest() {
        assertEquals(expected,  new Main().deleteLastWord(valueA));
    }


}
