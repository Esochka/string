package com.test;

import com.company.Main;
import org.junit.Assert;
import org.junit.Test;

public class mainTest {
    @Test
    public void testIntToStringTest() {
        String actual = "ку ві";

        String expected = Main.oneEks("ку віві");

        Assert.assertEquals(actual, expected);

    }
}
