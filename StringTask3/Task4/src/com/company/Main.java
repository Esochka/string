package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(oneEks("куууу віві"));
    }

    public static String oneEks(String str){
        char[]chars = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        boolean repeatedChar;
        for (int i = 0; i < chars.length; i++) {
            repeatedChar = false;
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i]== chars[j]) {
                    repeatedChar = true;
                    break;
                }
            }
            if (!repeatedChar) {
                sb.append(chars[i]);
            }
        }
        return sb.toString();
    }
}
