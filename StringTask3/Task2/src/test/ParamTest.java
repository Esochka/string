package test;


import com.company.Task3;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParamTest {
    private String[] valueA;
    private int valueB;
    private String[] expected;

    public ParamTest(String[] valueA, int valueB, String[] expected) {
        this.valueA = valueA;
        this.valueB = valueB;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {new String[]{"sdfhhhhhhds","wed"},4 ,new String[]{"sdfhhhhh$$$","wed"}},
                {new String[]{"sdfhhhhhhds","wed", "1233"},4 ,new String[]{"sdfhhhhh$$$","wed","1$$$"}},

        });
    }

    @Test
    public void stringToIntTest() {
        assertEquals(expected,  new Task3().changeLastSymbols(valueA, valueB));
    }


}
