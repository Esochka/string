package test;


import com.company.Task4;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParamTest {
    private String valueA;
    private Double expected;

    public ParamTest(String valueA, Double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"5.2", 5.2},
                {"6.2", 6.2}

        });
    }

    @Test
    public void stringToIntTest() {
        assertEquals(expected,  new Task4().stringToDouble(valueA));
    }


}
