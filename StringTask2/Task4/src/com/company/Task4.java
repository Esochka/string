package com.company;

public class Task4 {

    public static void main(String[] args) {
        System.out.println(stringToDouble("5.2"));
    }

    public static Double stringToDouble(String a) {

        try {
            return Double.parseDouble(a);

        } catch (NumberFormatException e) {
            return 0.0;
        }
    }
}
