package com.test;

import com.company.Main;
import org.junit.Assert;
import org.junit.Test;

public class mainTest {
    @Test
    public void testIntToStringTest() {
        int actual = 5;

        int expected = Main.stringToInt("5");

        Assert.assertEquals(actual, expected);

    }
}
