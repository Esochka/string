package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(stringToInt("5"));
    }

    public static int stringToInt(String a) {

        try {
            return Integer.parseInt(a);
        } catch (NumberFormatException e) {
            return 0;
        }

    }
}
